from flask import Flask, request, jsonify

from db import ATM

app = Flask(__name__)

CUR = ('RUB', 'USD', 'EUR')


@app.route("/deposit", methods=['POST'])
def deposit():
    if not (request.json
            and ('currency' in request.json and request.json['currency'] in CUR)
            and ('value' in request.json and request.json['value'] > 0)
            and ('quantity' in request.json and request.json['quantity'] > 0)):
        return jsonify({"code": 400, "error": "Not valid input data"}), 400

    banknotes = ATM.get_or_none(ATM.currency == request.json['currency'], ATM.value == request.json['value'])
    if banknotes:
        banknotes.quantity += request.json['quantity']
    else:
        banknotes = ATM(
            currency=request.json['currency'],
            value=request.json['value'],
            quantity=request.json['quantity']
        )
    banknotes.save()
    return jsonify({"success": True})


@app.route("/withdraw", methods=['POST'])
def withdraw():
    if not (request.json
            and ('currency' in request.json and request.json['currency'] in CUR)
            and ('amount' in request.json and request.json['amount'] > 0)):
        return jsonify({"code": 400, "error": "Not valid input data"}), 400
    currency_banknotes = [
        {
            'currency': item.currency,
            'value': item.value,
            'quantity': item.quantity,
        } for item in ATM.select().where(ATM.currency == request.json['currency']).order_by(ATM.value.desc())
    ]

    sum = 0
    for item in currency_banknotes:
        sum += item['value']*item['quantity']
    if sum < request.json['amount']:
        return jsonify({"code": 412, "error": "There are not enough funds at the ATM"}), 412

    withdraw_banknotes = []
    sum_withdraw = request.json['amount']

    for item in currency_banknotes:
        quantity = sum_withdraw // item['value']
        if quantity == 0:
            continue
        if quantity <= item['quantity']:
            withdraw_banknotes.append({
                'currency': item['currency'],
                'value': item['value'],
                'quantity': quantity,
            })
            sum_withdraw -= quantity * item['value']
        else:
            withdraw_banknotes.append({
                'currency': item['currency'],
                'value': item['value'],
                'quantity': item['quantity'],
            })
            sum_withdraw -= item['quantity'] * item['value']

    if sum_withdraw == 0:
        for item in withdraw_banknotes:
            banknotes_atm = ATM.get(ATM.currency == request.json['currency'], ATM.value == item['value'])
            banknotes_atm.quantity -= item['quantity']
            banknotes_atm.save()
        return jsonify({"success": True, "result": withdraw_banknotes})
    else:
        return jsonify({"code": 412, "error": "There are not enough funds at the ATM"}), 412


if __name__ == "__main__":
    app.run(debug=True)
