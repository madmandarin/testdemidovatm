import os

from peewee import *
from playhouse.db_url import connect

db = connect(os.environ.get('TEST_DEMIDOV_ATM_DB') or 'sqlite:///default.db')


class BaseModel(Model):
    class Meta:
        database = db


class ATM(BaseModel):
    currency = TextField()
    value = IntegerField()
    quantity = IntegerField()


with db:
    db.create_tables([ATM])
